import logo from './logo.svg';
import './App.css';
import Carrusel from "./Carrusel";
import CarruselF from "./CarruselF";
import Contador from "./Contador";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        Carrusel
      </header>
      <body>
      <p>Carrusel usando clase</p>
      {/**
       * Acá incluyo el carrusel con 4 hijos:
       * - un div 
       * - una imagen
       * - un párrafo
       * - un formulario
       */}
      <Carrusel>
        <div>Esto es un div</div>
        <img src={logo} width="30" height="30" alt="Imagen no encontrada"/>
        <p>Esto es un párrafo</p>
        <form>
          <label for="name">Nombre:</label>
          <input type="text" id="name" name="user_name"/>
        </form>
      </Carrusel>
      <hl/>
      <p>Carrusel usando función</p>

      {/**
       * Acá incluyo el carrusel con 4 hijos:
       * - un div 
       * - una imagen
       * - un párrafo
       * - un formulario
       */}
      <CarruselF>
        <div>Esto es un div B</div>
        <img src={logo} width="30" height="30" alt="Imagen no encontrada"/>
        <p>Esto es un párrafo</p>
        <form>
          <label for="name">Nombre:</label>
          <input type="text" id="name" name="user_name"/>
        </form>
      </CarruselF>
      <hl/>
      <p>Contador con botones</p>
       <Contador/>

      </body>
    </div>
  );
}

export default App;
