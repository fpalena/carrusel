import React, { useState, useEffect } from "react";

/**
 * Componente con un contador que se incrementa o decrementa en 1
 */
function Contador () {
  const [valor, setValor] = useState(0);

  useEffect(() => {
    alert("objeto montado");
    return () => {
      alert("objeto desmontado");
    };
  },[]);

  /**
   * Suma 1 al valor actual
   */
  const suma = () => {
    setValor(valor+1);
    alert(`Nuevo valor: ${valor+1}`);
  };

  /**
   * Resta 1 al valor actual
   */
  const resta = () => {
    setValor(valor-1);
    alert(`Nuevo valor: ${valor-1}`);
  };

  return (
    <div>
      <button onClick={suma}>Suma</button>
      <button onClick={resta}>Resta</button>
      
      <p>Valor {valor}</p>
      
    </div>
  )
}

export default Contador;