import React, { useState } from "react";

/**
 * 
 */
function CarruselF ({children}) {
  const [currentIndex, setCurrentIndex] = useState(0);
  
  const handleNext = () => {
    setCurrentIndex((currentIndex + 1) % children.length);
  };

  const handlePrev = () => {
    setCurrentIndex(currentIndex === 0 ? children.length - 1 : currentIndex - 1);
  };

  return (
    <div>
      <button onClick={handlePrev}>Atrás</button>{currentIndex}<button onClick={handleNext}>Adelante</button>
      <br/>
      <div>
      {children[currentIndex]}
      </div>
    </div>
  )
}

export default CarruselF;