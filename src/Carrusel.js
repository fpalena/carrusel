import React, { Component } from "react";

/**
 * 
 */
class Carrusel extends Component {
    /**
     * currentIndex: representa la posición actual del carrusel
     */
    state = {
        currentIndex: 0,
    };
    
    /**
     * Retrocede en el carrusel
     */
    handleNext = () => {
        const { children } = this.props;
        this.setState((prevState) => ({
          currentIndex:
            prevState.currentIndex === children.length - 1
              ? 0
              : prevState.currentIndex + 1,
        }));
      };
 
      /**
       * Avanza en el carrusel
       */
      handlePrev = () => {
        const { children } = this.props;
        this.setState((prevState) => ({
          currentIndex:
            prevState.currentIndex === 0
              ? children.length - 1
              : prevState.currentIndex - 1,
        }));
      };

      render() {
        const { children } = this.props;
        const { currentIndex } = this.state;
    
        return (
          <div>
            <button onClick={this.handlePrev}>Atrás</button>{currentIndex}<button onClick={this.handleNext}>Adelante</button>
            <br/>
            <div>
            {children[currentIndex]}
            </div>
          </div>
        );
      }
    }
    
    export default Carrusel;